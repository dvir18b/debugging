#include "Point.h"
#include <iostream>

int main()
{
	Point p1(1, 2);
	Point p2 = p1;

	std::cout << "p1=" << p1 << std::endl; // need to call the p1 << op
	std::cout << "p2=" << p2 << std::endl;

	return 0;
}