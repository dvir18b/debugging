#include <iostream>

struct Password
{
	char value[16]; // writing more than 16 characters, making the '\0' overriding the next value on the struct (they are after each other in the memory) and making the incorrect bool false
	// possible fix would be using std::string instead of char array
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl;
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********"))
		pwd.incorrect = false;

	if (!pwd.incorrect)
		std::cout << "Congratulations\n";

	return 0;
}