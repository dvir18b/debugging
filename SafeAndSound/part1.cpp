#include "part1.h"
#include <iostream>

// Copy characters from src to dest, limiting the copy to destsize characters.
// Ensure null-termination of the destination string.
char* string_copy(char* dest, unsigned int destsize, const char* src)
{
	char* ret = dest;

	while (*src && destsize > 1) {
		*dest++ = *src++;
		destsize--;
	}

	*dest = '\0';

	return ret;
}

void part1()
{
	char password[] = "secret";
	char dest[12];
	char src[] = "hello world!";

	string_copy(dest, 12, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}